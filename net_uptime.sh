#!/bin/bash

# 02/02/2018
# script to check network connection
#
# project tree:
#       |_ ./connection/
#               |_ network_connection.sh
#                _ log/
#                       |_ debug.log
#                       |_ fail.log
#               |_ database/ (TODO)
#
# 2 cron jobs:
#               1. the script itself that runs every minute;
#               2. the fail log that has to be sent every 3 months; tar?

# TODO
# check for nc and install it
# check curl and install it
# suppress output of ping maybe?!
# overwrite debug files every three months



GW=`/sbin/ip route | awk '/default/ { print $3 }'`
checkdns=`cat /etc/resolv.conf | awk '/nameserver/ {print $2}' | awk 'NR == 1 {print; exit}'`
checkdomain=google.com
checkip=8.8.8.8
debug_log="/home/user1/connection/log/debug.log"
fail_log="/home/user1/connection/log/fail.log"
cron_log="/home/user1/connection/log/cron.log" # this can be activated in the /etc/cron.d/network_uptime

# functions

function portscan
{
  if nc -zw1 ${checkdomain} 80 
  then
        # do nothing?! HTTP working fine
    echo "$(date "+%d%m%Y %T") : portscan test positive" >> ${debug_log} 2>&1
  else
    echo "$(date "+%d%m%Y %T") : HTTP port scan of ${checkdomain} failed." >> ${fail_log} 2>&1
  fi
}

function pingnet
{
  #Google has the most reliable host name. Feel free to change it.
  ping $checkdomain -c 4

  if [ $? -eq 0 ]
  then
      #Insert any command you like here; ping working fine
          echo "$(date "+%d%m%Y %T") : pingnet test positive" >> ${debug_log} 2>&1
    else
      echo "$(date "+%d%m%Y %T") : Ping check failed. Can't reach Google." >> ${fail_log} 2>&1
          #Insert any command you like here
#      exit 1
  fi
}

#function pingdns
#{
  #Grab first DNS server from /etc/resolv.conf
#  tput setaf 6; echo "Pinging first DNS server in resolv.conf ($checkdns) to check name resolution" && echo; tput sgr0;
#  ping $checkdns -c 4
#    if [ $? -eq 0 ]
#    then
      #tput setaf 6; echo && echo "$checkdns pingable. Proceeding with domain check."; tput sgr0;
      #Insert any command you like here; dns working;
#    else
#      echo && echo "$(date "+%m%d%Y %T") : Could not establish internet connection to DNS. Something may be wrong here." >&2
      #Insert any command you like here
#     exit 1
#  fi
#}

#function httpreq
#{
#  tput setaf 6; echo && echo "Checking for HTTP Connectivity"; tput sgr0;
#  case "$(curl -s --max-time 2 -I $checkdomain | sed 's/^[^ ]*  *\([0-9]\).*/\1/; 1q')" in
#  [23]) tput setaf 2; echo "HTTP connectivity is up"; tput sgr0;;
#  5) echo "The web proxy won't let us through";exit 1;;
#  *)echo "Something is wrong with HTTP connections. Go check it."; exit 1;;
#  esac
#  exit 0
#}


#Ping gateway first to verify connectivity with LAN
if [ "$GW" = "" ]; then
        echo "There is no gateway. L2 is down." >> ${fail_log} 2>&1
fi

ping $GW -c 4

if [ $? -eq 0 ]
then
  #pingdns
  pingnet
  portscan
  #httpreq
  exit 0
else
  echo "$(date "+%d%m%Y %T") : Something is wrong with LAN (Gateway unreachable)" >> ${fail_log} 2>&1
  #pingdns
  pingnet
  portscan
  #httpreq
fi


